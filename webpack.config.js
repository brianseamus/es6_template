const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: "./src/main.js",
  mode: 'development',
  module: {
    rules: [
      {
	test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: { presets: ['env'] }
      },
      {
	test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  resolve: { extensions: ['*', '.js', '.jsx'] },
  output: {
    path: path.resolve(__dirname, 'Dist/'),
    publicPath: '/Dist/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, 'Public/'),
    port: 3000,
    publicPath: 'http://localhost:3000/Dist/',
    hotOnly: true
  },
  plugins: [ new webpack.HotModuleReplacementPlugin() ]
};
